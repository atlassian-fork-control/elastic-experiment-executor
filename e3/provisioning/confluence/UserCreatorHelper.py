#!/usr/bin/env python

import logging
import math
import xmlrpclib
from threading import Thread


class UserCreatorHelper:
    def __init__(self, total_user, base_url, auth_user=("admin", "admin"), limit_for_single_thread=50):
        self._log = logging.getLogger('provision')
        self._total_user = total_user
        self._base_url = base_url
        self._limit_for_single_thread = limit_for_single_thread
        self._auth_user = auth_user

        self._num_success_created_user = 0

    def get_num_success_created_user(self):
        return self._num_success_created_user

    def prepare_test_users(self):
        self._log.info("Preparing to create %d users" % self._total_user)
        threads = []
        self._log.info("Starting %d threads to create users" % math.ceil(self._total_user / 50))
        _slice = range(0, self._total_user, self._limit_for_single_thread)
        for i in range(len(_slice)):
            if i + 1 < len(_slice):
                end = _slice[i + 1]
            else:
                end = self._total_user

            start_index = _slice[i]
            end_index = end
            is_break = False
            # only allow 4 threads to create user. The last one will handle all remaining users
            if len(threads) >= 3:
                end_index = self._total_user
                is_break = True
            threads.append(self._create_thread(instance_count=len(threads) + 1,
                                               start_index=start_index,
                                               total_user=end_index))
            if is_break:
                break

        self._log.info("Waiting for %d create user threads to complete" % len(threads))
        for thread in threads:
            thread.join()
            self._num_success_created_user += thread.num_success_created_user

        self._log.info("Finished creating %d users" % self._total_user)

    def _create_thread(self, instance_count, start_index, total_user):
        user_create_thread = UserCreationThread(base_url=self._base_url,
                                                auth_user=self._auth_user,
                                                instance_count=instance_count,
                                                start_index=start_index,
                                                total_user=total_user)
        user_create_thread.start()
        return user_create_thread


class UserCreationThread(Thread):
    def __init__(self, base_url, auth_user, instance_count, start_index, total_user, test_group_name="test-user-group"):
        Thread.__init__(self, name='UserCreationWorker:%d' % instance_count)
        self._log = logging.getLogger('provision')
        self._base_url = base_url
        self._auth_user = auth_user
        self._start_index = start_index
        self._total_user = total_user
        self._test_group_name = test_group_name
        self._max_retry_times = 3
        self.num_success_created_user = 0

    def run(self):
        confluence_xmlrpc = xmlrpclib.ServerProxy(self._base_url + "/rpc/xmlrpc")
        confluence_xmlrpc = confluence_xmlrpc.confluence2

        self._retry_action(self._max_retry_times, lambda: self._create_group(confluence_xmlrpc))
        for i in range(self._start_index, self._total_user):
            if self._retry_action(self._max_retry_times, lambda: self._create_user(confluence_xmlrpc, "test%s" % i)):
                ++self.num_success_created_user

    def _create_user(self, confluence_xmlrpc, user_name):
        rpc_token = confluence_xmlrpc.login(self._auth_user[0], self._auth_user[1])

        # create user if they don't already exist
        if not confluence_xmlrpc.hasUser(rpc_token, user_name):
            self._create_new_user(confluence_xmlrpc, user_name, self._test_group_name, rpc_token)

        self._log.debug("User [%s] created", user_name)
        return user_name

    def _create_group(self, confluence_xmlrpc):
        rpc_token = confluence_xmlrpc.login(self._auth_user[0], self._auth_user[1])
        if not confluence_xmlrpc.hasGroup(rpc_token, self._test_group_name):
            self._log.info("Creating test user group")
            confluence_xmlrpc.addGroup(rpc_token, self._test_group_name)

    def _retry_action(self, num_retry, delegate_action):
        """
        retry action for number of time until it success
        :param num_retry:
        :param delegate_action:
        :return:
        """
        for i in range(0, num_retry):
            try:
                if delegate_action:
                    delegate_action()
                    return True
            except Exception as ex:
                if i == num_retry - 1:
                    self._log.error("Exception while preparing users: %s" % ex)
                pass

        return False

    @staticmethod
    def _create_new_user(confluence_xmlrpc, user_name, group_name, rpc_token):
        confluence_xmlrpc.addUser(
            rpc_token,
            {
                "email": "%s@atlassian.net" % user_name,
                "fullname": user_name,
                "name": user_name,
                "url": "/admin/users/viewuser.action?username=%s" % user_name
            },
            user_name)

        confluence_xmlrpc.addPersonalSpace(rpc_token, {"name": user_name, "key": "~%s" % user_name}, user_name)
        confluence_xmlrpc.addPermissionToSpace(rpc_token, "VIEWSPACE", "confluence-users", "~%s" % user_name)
        confluence_xmlrpc.addUserToGroup(rpc_token, user_name, group_name)


if __name__ == '__main__':
    test = UserCreatorHelper(base_url="http://localhost:8080/confluence", total_user=9, limit_for_single_thread=10)
    test.prepare_test_users()
