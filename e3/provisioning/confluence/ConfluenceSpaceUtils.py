import httplib
import xmlrpclib
import requests
import json
import base64
import logging


def read_file_as_bytes(filename):
    file = open(filename, "rb")
    handle = file.read()
    file.close()

    return handle


def reset_seed_space(space_key, base_url, filename, auth_user, auth_password):
    (confluence_xmlrpc, rpc_token) = authenticate_rpc(base_url, auth_user, auth_password)
    remove_space(confluence_xmlrpc, rpc_token, space_key)
    return import_space(confluence_xmlrpc, rpc_token, filename)


def authenticate_rpc(base_url, auth_user="admin", auth_password="admin"):
    import urllib
    uri = base_url + "/rpc/xmlrpc"
    type, uri = urllib.splittype(uri)
    if type == "https":
        transport = SafeTimeoutTransport(use_datetime=0, context=None)
    else:
        transport = TimeoutTransport(use_datetime=0)
    confluence_xmlrpc = xmlrpclib.ServerProxy(base_url + "/rpc/xmlrpc", transport=transport)

    confluence_xmlrpc = confluence_xmlrpc.confluence2

    rpc_token = confluence_xmlrpc.login(auth_user, auth_password)
    logging.debug("XmlRpc login token: %s" % rpc_token)

    return confluence_xmlrpc, rpc_token


def import_space(confluence_xmlrpc, token, filename):
    space_file = read_file_as_bytes(filename)
    return confluence_xmlrpc.importSpace(token, xmlrpclib.Binary(space_file))


def get_global_spaces(base_url, token, auth_user, auth_password):
    response = requests.get("%s/rest/api/space" % base_url.strip('/'),
                            {'os_authType': 'basic', 'type': 'global'},
                            headers={
                                'Authorization': "Basic %s" % base64.b64encode("%s:%s" % (auth_user, auth_password))})
    response_json = json.loads(response.text)

    return response_json["results"]


def remove_global_spaces(base_url, confluence_xmlrpc, token):
    spaces = get_global_spaces(base_url, token)
    for space in spaces:
        logging.debug("Removing space: %s" % space["key"])
        remove_space(confluence_xmlrpc, token, str(space["key"]))


def remove_space(confluence_xmlrpc, token, space_key):
    # sometimes space removal hangs. that may happen because of "funny" percentage calculation (deletion may be done,
    # but label keeps saying "99% complete"). in this case we would probably need to timeout and re-try.
    count = 0
    while count < 3:  # re-trying 3 times
        try:
            return confluence_xmlrpc.removeSpace(token, space_key)
        except:
            logging.debug("%s: removing space failed!" % space_key)
            count += 1
    return "Operation failed!"


def has_space(confluence_rpc, token, space_key):
    return confluence_rpc.getSpace(token, space_key) is not None


class TimeoutTransport(xmlrpclib.Transport):
    timeout = 600  # 10 minutes

    def set_timeout(self, timeout):
        self.timeout = timeout

    def make_connection(self, host):
        # return an existing connection if possible.  This allows
        # HTTP/1.1 keep-alive.
        if self._connection and host == self._connection[0]:
            return self._connection[1]

        # create a HTTP connection object from a host descriptor
        chost, self._extra_headers, x509 = self.get_host_info(host)
        # store the host argument along with the connection object
        self._connection = host, httplib.HTTPConnection(chost, timeout=self.timeout)
        return self._connection[1]


class SafeTimeoutTransport(xmlrpclib.SafeTransport):
    """Handles an HTTPS transaction to an XML-RPC server."""
    timeout = 600  # 10 minutes

    def set_timeout(self, timeout):
        self.timeout = timeout

    def make_connection(self, host):
        if self._connection and host == self._connection[0]:
            return self._connection[1]
        # create a HTTPS connection object from a host descriptor
        # host may be a string, or a (host, x509-dict) tuple
        try:
            HTTPS = httplib.HTTPSConnection
        except AttributeError:
            raise NotImplementedError(
                "your version of httplib doesn't support HTTPS"
            )
        else:
            chost, self._extra_headers, x509 = self.get_host_info(host)
            self._connection = host, HTTPS(chost, None, context=self.context, timeout=self.timeout, **(x509 or {}))
            return self._connection[1]
